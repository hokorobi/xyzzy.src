■使用条件

LICENSE を参照のこと。

■解説

ソースからビルドしてみる3
https://www.sugarsync.com/referral?rf=c86z6gajs0qc
をの手順をまとめました。

Unicode Consortium のファイルも
MITライセンスでOKだと確認をもらったので置いてます。

etc,html,info,lisp,site-lisp は標準のexeと一緒に
配布されているのそのままです。
コンパイルが終わった後にサイトから
落としてくるのがめんどくさいので置いてます。

コンパイル方法

1 Microsoft Visual C++ 2008 Express Edition インストール
2 env.vbs を叩く (環境変数のセットとafxres.hのコピー)
3 cd xyzzy/src
4 nmake              (デバッグ版は nmake CFG=d)
5 ぽけーと待つ
6 できあがり

■連絡先
2chのスレ、もしくは http://twitter.com/atomfe へ。