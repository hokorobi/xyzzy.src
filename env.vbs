Set objShell = WScript.CreateObject("WScript.Shell")
Set colEnvUser = objShell.Environment("User")

Dim newLib
Dim newInclude
newLib = colEnvUser.Item("LIB") + "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\lib;" + "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Lib;"
newInclude = colEnvUser.Item("INCLUDE") + "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include;" + "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\include;"

colEnvUser.Item("LIB") = newLIB
colEnvUser.Item("INCLUDE") = newInclude

Set colEnvSys = objShell.Environment("System")

Dim newPath
newPath = colEnvSys.Item("PATH") + "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin;" + "C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE;" + "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin;"
colEnvSys.Item("PATH") = newPath

'copy afxres.h to proper location
Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
objFSO.CopyFile "afxres.h", "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include\", True

